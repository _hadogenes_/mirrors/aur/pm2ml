#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='pm2ml',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1637376062)),
  description='''Generate metalinks for downloading Pacman packages and databases.''',
  author='Xyne',
  author_email='gro xunilhcra enyx, backwards',
  url='''http://xyne.dev/projects/pm2ml''',
  py_modules=['''pm2ml'''],
  scripts=['pm2ml']
)
